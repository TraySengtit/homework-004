import { HStack, Stack, VStack, Text, HamburgerIcon, View } from 'native-base'
import React from 'react'
import { StyleSheet } from 'react-native'
import Ionicons from 'react-native-vector-icons/Ionicons';
import Feather from 'react-native-vector-icons/Feather';

const AbaHeader = () => {
    return (
        <View>
            <HStack justifyContent="space-between" bg="info.800" h="60">
                <HStack>
                    <VStack space={4} style={styles.centerStyle} ml="1">
                        <HamburgerIcon size="5" mx="2" color="primary.100" />
                    </VStack>
                    <VStack style={styles.centerStyle} >
                        <Text fontSize="2xl" fontWeight="bold" ml="1" color="primary.100" letterSpacing="lg">ABA Mobile</Text>
                    </VStack>
                </HStack>
                <HStack>
                    <VStack style={styles.centerStyle}>
                        <Ionicons name="notifications-outline" size={25} style={{ color: "whitesmoke" }} />
                    </VStack>
                    <VStack style={styles.centerStyle} mx="5">
                        <Feather name="phone-call" size={22} style={{ color: "whitesmoke" }} />
                    </VStack>
                </HStack>
            </HStack>
        </View>
    )
}
const styles = StyleSheet.create({
    centerStyle: {
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor: "purple",
    }
})
export default AbaHeader
