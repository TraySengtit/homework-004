import React from 'react'
import { StyleSheet } from 'react-native'
import { HStack, Stack, VStack, Text, View } from 'native-base';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';



const AbaContent = () => {
    return (
        <View h="450" bg="info.900" flex={1}>
            <HStack flex={1}  style={styles.content}>
                    <VStack flex={1} h="100%" alignItems="center" justifyContent="center"  style={{borderRightWidth: 0.3, borderColor: '#eef2ff'}}>
                        <FontAwesome5 name="wallet" size={36} style={{ color: "whitesmoke" }}/>
                        <Text style={{ color: "whitesmoke" }}>Account</Text>
                    </VStack>
                    <VStack flex={1} h="100%" alignItems="center" justifyContent="center">
                        <Ionicons name="card" size={38} style={{ color: "whitesmoke" }}/>
                        <Text style={{ color: "whitesmoke" }}>Card</Text>
                    </VStack>
                    <VStack flex={1} h="100%" alignItems="center" justifyContent="center"  style={{borderLeftWidth: 0.3, borderColor: '#eef2ff'}}>
                        <MaterialCommunityIcons name="currency-usd-circle" size={38} style={{ color: "whitesmoke" }}/>
                        <Text style={{ color: "whitesmoke" }}>Payments</Text>
                    </VStack>
            </HStack>

            <HStack flex={1}  style={styles.content}>
                    <VStack flex={1} h="100%" alignItems="center" justifyContent="center"  style={{borderTopWidth: 0.3, borderColor: '#eef2ff'}}>
                        <MaterialCommunityIcons name="file-document-edit" size={38} style={{ color: "whitesmoke" }}/>
                        <Text style={{ color: "whitesmoke" }}>New Account</Text>
                    </VStack>
                    <VStack flex={1} h="100%" alignItems="center" justifyContent="center" style={{borderTopWidth: 0.3, borderLeftWidth: 0.3, borderRightWidth: 0.3, borderColor: '#eef2ff'}}>
                        <MaterialCommunityIcons name="cash-usd" size={38} style={{ color: "whitesmoke" }}/>
                        <Text style={{ color: "whitesmoke" }}>Cash to ATM</Text>
                    </VStack>
                    <VStack flex={1} h="100%" alignItems="center" justifyContent="center"  style={{borderTopWidth: 0.3, borderColor: '#eef2ff'}}>
                        <MaterialCommunityIcons name="swap-horizontal-circle" size={38} style={{ color: "whitesmoke" }}/>
                        <Text style={{ color: "whitesmoke" }}>Transfers</Text>
                    </VStack>
            </HStack>

            <HStack flex={1}  style={styles.content}>
                    <VStack flex={1} h="100%" alignItems="center" justifyContent="center" style={{borderWidth: 0.2, borderColor: '#eef2ff'}}>
                        <MaterialCommunityIcons name="qrcode-scan" size={38} style={{ color: "whitesmoke" }}/>
                        <Text style={{ color: "whitesmoke" }}>Scan QR</Text>
                    </VStack>
                    <VStack flex={1} h="100%"  alignItems="center" justifyContent="center" style={{borderTopWidth: 0.3, borderColor: '#eef2ff'}}>
                        <FontAwesome5 name="hand-holding-usd" size={38} style={{ color: "whitesmoke" }}/>
                        <Text style={{ color: "whitesmoke" }}>Loans</Text>
                    </VStack>
                    <VStack flex={1} h="100%"  alignItems="center" justifyContent="center" style={{borderWidth: 0.2, borderColor: '#eef2ff'}}>
                        <Entypo name="location" size={38} style={{ color: "whitesmoke" }}/>
                        <Text style={{ color: "whitesmoke" }}>Locator</Text>
                    </VStack>
            </HStack>

        </View>
    )
}
const styles = StyleSheet.create({
    content:{
        flexDirection: "row",    },
    contentItem: {
        flexDirection: 'row',
        width: '100%',
        height: '50%',
        alignItems: 'center',
        justifyContent: 'flex-start'
    }
})
export default AbaContent
