import { HStack, VStack, Text, View } from 'native-base'
import React from 'react'
// import { View } from 'react-native'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

const AbaFooter = () => {
    return (
        <View>
            <HStack bg="primary.400">
                <VStack flex={3} w="64" h="40"  justifyContent='center' mr="2">
                    <Text fontSize="xl" fontWeight="bold" ml="5" color="primary.100">Quick Transfer</Text>
                    <Text ml="5" fontSize="xs" color="primary.100">Create your templates here to make Transfer quicker</Text>
                </VStack>
                <VStack flex={1} alignSelf="flex-end">
                    <MaterialCommunityIcons name="swap-horizontal-circle" size={124} style={{ color: "whitesmoke", marginBottom: -24,}} />
                </VStack>
            </HStack>
            <HStack bg="red.400">
                <VStack flex={3} w="64" h="40"  justifyContent='center' mr="2">
                    <Text fontSize="xl" fontWeight="bold" ml="5" color="primary.100">Quick Payment</Text>
                    <Text ml="5" fontSize="xs" color="primary.100">Paying your bills with templates is faster</Text>
                </VStack>
                <VStack flex={1} alignSelf="flex-end">
                    <MaterialCommunityIcons name="currency-usd-circle" size={124} style={{ color: "whitesmoke", marginBottom: -24 }}/>
                </VStack>
            </HStack>
        </View>
    )
}

export default AbaFooter
