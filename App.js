import { NativeBaseProvider } from 'native-base';
import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import AbaContent from './src/components/AbaContent';
import AbaFooter from './src/components/AbaFooter';
import AbaHeader from './src/components/AbaHeader';

const App = () => {
  return (
    <NativeBaseProvider style={styles.container}>
      <AbaHeader/>
      <AbaContent/>
      <AbaFooter/>
    </NativeBaseProvider>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    // width: '100%',
    // height: '100%'
  }
})
export default App;
